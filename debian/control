Source: python-ptk
Priority: optional
Maintainer: Nicolas Boulenguez <nicolas@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 python3-all,
 python3-async-generator,
 python3-sphinx,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Section: python
Homepage: https://github.com/fraca7/ptk
Vcs-Browser: https://salsa.debian.org/debian/python-ptk
Vcs-Git: https://salsa.debian.org/debian/python-ptk.git
Testsuite: autopkgtest-pkg-python

Package: python3-ptk
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: python3-async-generator
Suggests: python-ptk-doc, python3-twisted
Description: parser for Python 3 with support for asynchronous input
 PTK implements LR(1) parsing in Python. Compared to compiled tools
 like Bison, it attempts to spare programmer's time. Python sources
 describe both the grammar and the callbacks, avoiding code
 generation.  Various inputs are accepted: Python 3 asynchronous
 streams, PyZipFile archives, Twisted Deferred objects.
 .
 python3-async-generator is required for asynchronous streams.
 .
 This package installs the library for Python 3.

Package: python-ptk-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: parser for Python with support for asynchronous input (documentation)
 PTK implements LR(1) parsing in Python. Compared to compiled tools
 like Bison, it attempts to spare programmer's time. Python sources
 describe both the grammar and the callbacks, avoiding code
 generation.  Various inputs are accepted: Python 3 asynchronous
 streams, PyZipFile archives, Twisted Deferred objects.
 .
 This package contains the documentation.
